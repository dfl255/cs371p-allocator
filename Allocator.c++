#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <utility>  // make_pair, pair
#include <unordered_map> //for quick two pow lookups
#include <vector> //arraylist
#include <set> //set
#include "Allocator.h"

using namespace std;


vector<int> solve_case(vector<int> inputs)
{
    //template here shouldn't matter...
    my_allocator<double,1000> alloc;
    //vector<char*> takenBlocks;
    for(int i = 0; i < inputs.size(); i++)
    {
        int request = inputs.at(i);

        if(request > 0)
        {
            //allocate a block or handle exception
            //cout << "calling allocate" << endl;
            alloc.allocate(request);
            //alloc.printShit();

        }
        else
        {
            //free a block

            assert(request != 0);

            //make positive
            request*=-1;
            assert(request >= 1);

            //my_allocator::pointer p = (my_allocator::pointer)takenBlocks.at(request);

            int index = 0;
            auto iter = alloc.begin();

            //assume input is valid, and doesn't go forever
            while(index < request)
            {
                if(*iter < 0)
                {
                    index++;

                    //found the block to free
                    if(index == request)
                    {
                        //iter should be int*, fits since we gave int as template
                        //cout << "calling deallocate on block sized " << *iter << endl;
                        //cout << "calling deallocate on the second " << request << endl;
                        alloc.deallocate((double*)((char*)&*iter + 4), 0);
                        //alloc.printShit();
                        break;
                    }
                }

                iter++;
            }
            //takenBlocks.erase(takenBlocks.begin() + request);
        }
    }

    //std::cout << "completed all requests" << std::endl;

    //iterate through blocks for all the sentinels and add to vector
    vector<int> sentinels;
    auto iter = alloc.begin();

    //remember that &*iter is an int*
    while(!alloc.pastEnd(&*iter))
    {
        sentinels.push_back(*iter);
        //std::cout << "pushed back " << *iter;
        iter++;
    }
    //std::cout << "sentinels found" << std::endl;
    return sentinels;
}

void allocator_solve(istream& r, ostream& w)
{
    string s;
    getline(r,s);
    istringstream case_line(s);
    int cases;
    case_line >> cases;

    //eat a blank line
    getline(r,s);

    for(int i = 0; i < cases; i++)
    {
        //while we don't bump into new lines...
        vector<int> requests;
        string input_line;
        getline(r,input_line);
        //std::cout << input_line+"\n";

        while(input_line.length() >= 1)
        {
            int value;
            istringstream inputStream(input_line);
            //std::cout << input_line+"\n";
            //value = std::stoi(input_line);
            inputStream >> value;
            //w << value << "\n";
            requests.push_back(value);
            if(!getline(r,input_line))
            {
                break;
            }
        }

        //return results for this case
        vector<int> sentinels = solve_case(requests);
        //w << "!!!!!!!!!!!!!!!!!!!$*#(@!$(*#! RESULTS" << endl;
        for(int i = 0; i < sentinels.size(); i++)
        {
            //don't leave extra space at end of line
            if(i != sentinels.size()-1)
            {
                w << sentinels.at(i) << " ";
            }
            else
            {
                w << sentinels.at(i);
            }
        }

        //add new line for next case
        if(cases != 9)
            w << "\n";
        //cout<< i <<" ";
    }
    //cout << "wrapped up";
}




