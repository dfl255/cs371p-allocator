.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules

ifeq ($(shell uname -s), Darwin)
    ASTYLE        := astyle
    BOOST         := /usr/local/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := g++-9
    CXXFLAGS      := -fprofile-arcs -ftest-coverage -pedantic -std=c++17 -O3 -I/usr/local/include -Wall -Wextra
    LDFLAGS       := -lgtest -lgtest_main
    DOXYGEN       := doxygen
    GCOV          := gcov-9
    VALGRIND      := valgrind
else ifeq ($(shell uname -p), unknown)
    ASTYLE        := astyle
    BOOST         := /usr/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := g++
    CXXFLAGS      := -fprofile-arcs -ftest-coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    LDFLAGS       := -lgtest -lgtest_main -pthread
    DOXYGEN       := doxygen
    GCOV          := gcov
    VALGRIND      := valgrind
else
    ASTYLE        := astyle
    BOOST         := /usr/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := g++-9
    CXXFLAGS      := -fprofile-arcs -ftest-coverage -pedantic -std=c++17 -O3 -Wall -Wextra
    LDFLAGS       := -lgtest -lgtest_main -pthread
    DOXYGEN       := doxygen
    GCOV          := gcov-9
    VALGRIND      := valgrind
endif

FILES :=                                  \
    .gitignore                            \
    allocator-tests                         \
    Allocator.c++                           \
    Allocator.h                             \
    makefile                              \
    RunAllocator.c++                        \
    RunAllocator.in                         \
    RunAllocator.out                        \
    TestAllocator.c++                       \
    allocator-tests/dfl255-RunAllocator.in  \
    allocator-tests/dfl255-RunAllocator.out \
    Allocator.log                           \
    html                                  \


allocator-tests:
	git clone https://gitlab.com/gpdowning/cs371p-allocator-tests.git allocator-tests

html: Doxyfile Allocator.h
	$(DOXYGEN) Doxyfile

Allocator.log:
	git log > Allocator.log

Doxyfile:
	$(DOXYGEN) -g

RunAllocator: Allocator.h Allocator.c++ RunAllocator.c++
	-$(CPPCHECK) Allocator.c++
	-$(CPPCHECK) RunCAllocator.c++
	$(CXX) $(CXXFLAGS) Allocator.c++ RunAllocator.c++ -o RunAllocator

RunAllocator.c++x: RunAllocator
	./RunAllocator < RunAllocator.in > RunAllocator.tmp
	-diff RunAllocator.tmp RunAllocator.out

TestAllocator: Allocator.h Allocator.c++ TestAllocator.c++
	-$(CPPCHECK) Allocator.c++
	-$(CPPCHECK) TestAllocator.c++
	$(CXX) $(CXXFLAGS) Allocator.c++ TestAllocator.c++ -o TestAllocator $(LDFLAGS)

TestAllocator.c++x: TestAllocator
	$(VALGRIND) ./TestAllocator
	$(GCOV) -b Allocator.c++ | grep -A 5 "File '.*Allocator.c++'"

all: RunAllocator TestAllocator

check: $(FILES)

clean:
	rm -f *.gcda
	rm -f *.gcno
	rm -f *.gcov
	rm -f *.plist
	rm -f *.tmp
	rm -f RunAllocator
	rm -f TestAllocator

config:
	git config -l
	
ctd:
	$(CHECKTESTDATA) RunAllocator.ctd RunAllocator.in

docker:
	docker run -it -v $(PWD):/usr/gcc -w /usr/gcc gpdowning/gcc

format:
	$(ASTYLE) Allocator.c++
	$(ASTYLE) Allocator.h
	$(ASTYLE) RunAllocator.c++
	$(ASTYLE) TestAllocator.c++


init:
	git init
	git remote add origin git@gitlab.com:gpdowning/cs371p-allocator.git
	git add README.md
	git commit -m 'first commit'
	git push -u origin master

pull:
	make clean
	@echo
	git pull
	git status

push:
	make clean
	@echo
	git add .gitignore
	git add Allocator.h
	git add makefile
	git add README.md
	git add TestAllocator.c++
	git commit -m "another commit"
	git push
	git status
	
run: RunAllocator.c++x TestAllocator.c++x

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

sync:
	make clean
	@pwd
	@rsync -r -t -u -v --delete              \
    --include "Allocator.h"                  \
    --include "TestAllocator.c++"            \
    --exclude "*"                            \
    ~/projects/c++/allocator/ .
	@rsync -r -t -u -v --delete              \
    --include "makefile"                     \
    --include "Allocator.h"                  \
    --include "TestAllocator.c++"            \
    --exclude "*"                            \
    . downing@$(CS):cs/git/cs371p-allocator/

versions:
	@echo "% shell uname -p"
	@echo  $(shell uname -p)
	@echo
	@echo "% shell uname -s"
	@echo  $(shell uname -s)
	@echo
	@echo "% which $(ASTYLE)"
	@which $(ASTYLE)
	@echo
	@echo "% $(ASTYLE) --version"
	@$(ASTYLE) --version
	@echo
	@echo "% grep \"#define BOOST_VERSION \" $(BOOST)/version.hpp"
	@grep "#define BOOST_VERSION " $(BOOST)/version.hpp
	@echo
	@echo "% which $(CHECKTESTDATA)"
	@which $(CHECKTESTDATA)
	@echo
	@echo "% $(CHECKTESTDATA) --version"
	@$(CHECKTESTDATA) --version
	@echo
	@echo "% which $(CXX)"
	@which $(CXX)
	@echo
	@echo "% $(CXX) --version"
	@$(CXX) --version
	@echo "% which $(CPPCHECK)"
	@which $(CPPCHECK)
	@echo
	@echo "% $(CPPCHECK) --version"
	@$(CPPCHECK) --version
	@echo
	@$(CXX) --version
	@echo "% which $(DOXYGEN)"
	@which $(DOXYGEN)
	@echo
	@echo "% $(DOXYGEN) --version"
	@$(DOXYGEN) --version
	@echo
	@echo "% which $(GCOV)"
	@which $(GCOV)
	@echo
	@echo "% $(GCOV) --version"
	@$(GCOV) --version
ifneq ($(shell uname -s), Darwin)
	@echo "% which $(VALGRIND)"
	@which $(VALGRIND)
	@echo
	@echo "% $(VALGRIND) --version"
	@$(VALGRIND) --version
endif
