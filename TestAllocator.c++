// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <memory>    // allocator
#include <sstream>
#include <string>

#include "gtest/gtest.h"

#include "Allocator.h"

// --------------
// TestAllocator1
// --------------

template <typename A>
struct TestAllocator1 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
std::allocator<int>,
    std::allocator<double>,
    my_allocator<int,    100>,
    my_allocator<double, 100>>
    my_types_1;

TYPED_TEST_CASE(TestAllocator1, my_types_1);

TYPED_TEST(TestAllocator1, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator1, test_2) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// --------------
// TestAllocator2
// --------------

template <typename A>
struct TestAllocator2 : testing::Test {
    // --------
    // typedefs
    // --------

    typedef          A             allocator_type;
    typedef typename A::value_type value_type;
    typedef typename A::size_type  size_type;
    typedef typename A::pointer    pointer;
};

typedef testing::Types<
my_allocator<int,    100>,
             my_allocator<double, 100>>
             my_types_2;

TYPED_TEST_CASE(TestAllocator2, my_types_2);

TYPED_TEST(TestAllocator2, test_1) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(v, *p);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(TestAllocator2, test_2) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(s, std::count(b, e, v));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TYPED_TEST(TestAllocator2, test_3) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    ASSERT_EQ(x[0], 92);
}

TYPED_TEST(TestAllocator2, test_4) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    const allocator_type x;
    ASSERT_EQ(x[0], 92);
}

TYPED_TEST(TestAllocator2, test_5) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;
    allocator_type x;
    pointer j = x.allocate(2);
    x.construct(j, 2);
    x.destroy(j);
    x.deallocate(j, 2);
    ASSERT_EQ(x[0], 92);
}

TYPED_TEST(TestAllocator2, test_6) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    pointer j = x.allocate(4);
    x.construct(j, 4);
    x.destroy(j);
    x.deallocate(j, 4);
    ASSERT_EQ(x[0], 92);
}

TYPED_TEST(TestAllocator2, test_7) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    pointer j = x.allocate(8);
    x.construct(j, 8);
    x.destroy(j);
    x.deallocate(j, 8);
    ASSERT_EQ(x[0], 92);
}

TYPED_TEST(TestAllocator2, test_8) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    pointer j = x.allocate(3);
    x.construct(j, 3);
    x.destroy(j);
    x.deallocate(j, 3);
    ASSERT_EQ(x[0], 92);
}

TYPED_TEST(TestAllocator2, test_9) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    pointer j = x.allocate(10);
    x.construct(j, 10);
    x.destroy(j);
    x.deallocate(j, 10);
    ASSERT_EQ(x[0], 92);
}

TYPED_TEST(TestAllocator2, test_10) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    pointer j = x.allocate(6);
    x.construct(j, 6);
    x.destroy(j);
    x.deallocate(j, 6);
    ASSERT_EQ(x[0], 92);
}

TYPED_TEST(TestAllocator2, test_11) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    pointer j = x.allocate(9);
    x.construct(j, 9);
    x.destroy(j);
    x.deallocate(j, 9);
    ASSERT_EQ(x[0], 92);
}

TYPED_TEST(TestAllocator2, test_12) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    pointer j = x.allocate(11);
    x.construct(j, 11);
    x.destroy(j);
    x.deallocate(j, 11);
    ASSERT_EQ(x[0], 92);
}

TYPED_TEST(TestAllocator2, test_13) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    pointer j = x.allocate(1);
    x.construct(j, 1);
    x.destroy(j);
    x.deallocate(j, 1);
    ASSERT_EQ(x[0], 92);
}

TYPED_TEST(TestAllocator2, test_14) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    pointer j = x.allocate(7);
    x.construct(j, 7);
    x.destroy(j);
    x.deallocate(j, 7);
    ASSERT_EQ(x[0], 92);
}

TYPED_TEST(TestAllocator2, test_15) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    allocator_type x;
    pointer j = x.allocate(4);
    x.construct(j, 4);
    x.destroy(j);
    x.deallocate(j, 4);
    ASSERT_EQ(x[0], 92);
}

/*TYPED_TEST(TestAllocator2, test_16) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    string in = "1\n\n2\n3\n-1\n10\n-2\n8\n-1\n4\n-1\n1";
    sstream::istringstream r(in);
    sstream::ostringstream w;
    allocator_solve(r,w);
    ASSERT_EQ(w.str(), "-8 32 -64 864\n");}

TYPED_TEST(TestAllocator2, test_17) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    string in = "1\n\n2\n2\n-1\n7\n-2\n5\n-2\n8\n7\n8";
    istringstream r(in);
    ostringstream w;
    allocator_solve(r,w);
    ASSERT_EQ(w.str(), "16 -16 -64 -56 -64 736");}

TYPED_TEST(TestAllocator2, test_18) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    string in = "1\n\n7\n2\n-1\n2\n6\n-3\n8\n6\n4\n-4";
    istringstream r(in);
    ostringstream w;
    allocator_solve(r,w);
    ASSERT_EQ(w.str(), "-16 -32 -16 64 -48 776");}

TYPED_TEST(TestAllocator2, test_19) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    string in = "1\n\n5\n4\n-2\n7\n6\n5\n8\n3\n-1\n-3";
    istringstream r(in);
    ostringstream w;
    allocator_solve(r,w);
    ASSERT_EQ(w.str(), "40 -56 -48 40 -64 -24 672");}

TYPED_TEST(TestAllocator2, test_20) {
    typedef typename TestFixture::allocator_type allocator_type;
    typedef typename TestFixture::value_type     value_type;
    typedef typename TestFixture::size_type      size_type;
    typedef typename TestFixture::pointer        pointer;

    string in = "1\n\n10\n5\n-1\n9\n4\n-3\n-1\n9\n-1\n10";
    istringstream r(in);
    ostringstream w;
    allocator_solve(r,w);
    ASSERT_EQ(w.str(), "-80 -40 856");}*/