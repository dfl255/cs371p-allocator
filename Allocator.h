// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h

// --------
// includes
// --------

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument

#include <iostream> //streams for processing input

//adding methods in .c++ file
void allocator_solve(std::istream& r, std::ostream& w);

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * Check that all blocks meet min size, adjacent free blocks are merged, and sentinels are labeled/in right pos
     */
    bool valid () const {
        iterator c((int*)(&a[4]));
        //std::cout<<"valid called";
        while(true)
        {
            //std::cout<<"made it past the if!"<< *c << std::endl;
            //check that sentinels are equal and in correct pos
            //*((int*)((char*)&*c + 4
            char* temp = (char*)(&*c + 1);
            //std::cout<<"help";
            //std::cout << *(int*)((long)(temp + abs(*c)));
            if(*c != *(int*)((long)(temp + abs(*c))))
            {

                //std::cout<<"SENTINELS not equal! sentinels are:"<<*c<<" and "<<*(int*)((long)(temp + abs(*c)))<<std::endl;
                //std::cout<<&*c;
                std::cout<<(int*)a;
                //std::cout<<" \n";
                //std::cout<<(long)((char*)a);
                //std::cout<<" "+(long)((char*)&*c);
                return false;
            }

            //check that min size is met
            //since block already exists, no need to check size for sentinels
            if(abs(*c) < sizeof(T))
            {
                //std::cout<<"c sent address is "<<&*c<<std::endl;
                //std::cout<<"min size not met! current size is "<<abs(*c)<<", while the size of T is "<<sizeof(T)<<std::endl;
                return false;
            }


            //if free, check that adjacent blocks are merged
            if(!atEnd(&*c) && *c > 0)
            {
                //check after, no need to check before since we iterate from the start...
                //check that we're not last block
                //std::cout << (long)((char*)(&*c + 1) + abs(*c));
                //return false;
                //std::cout << (long)&a[N-4];
                //return false;
                if((long)((char*)(&*c + 1) + abs(*c)) != (long)&a[N-4])
                {
                    //next block is also free (pos sentinel val)

                    //NOT SAFE PLEASE CHECK THIS?
                    if(*((int*)((char*)(&*c + 2) + *c)) > 0)
                    {
                        //std::cout<<"adjacent free blocks"<<std::endl;
                        //std::cout<<*((int*)((char*)(&*c + 2) + *c))<<std::endl;
                        //std::cout<<"adjacent free blocks"<<std::endl;
                        //printShit();
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
            //std::cout << "looping " << atEnd(&*c) << std::endl;
            if(atEnd(&*c))
                break;
            ++c;


        }
        //std::cout<<"return true";
        return true;
    }


    static int abs(int a)
    {
        int b = a;
        if(b<0)
            b = a*-1;
        return b;
    }
public:
    // --------
    // iterator
    // --------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            //not quite sure why it wouldn't just be opposite of !=
            return lhs._p == rhs._p;
        }                                           // replace!

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            //std::cout<<"iter pointer: ";
            //std::cout << (long)p;
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            //std::cout<<"location of *c";
            //std::cout << *(_p - 1);
            return *(_p - 1);//*(int*)(((long)_p) - 4);
        }

        // -----------
        // operator ++
        // -----------

        //move to next block
        iterator& operator ++ () {
            //add block size to our block pointer, then add 2x sentinels worth to get to next block
            int* nxtBlockAddr = (int*)((char*)(_p + 2) + abs(**this));
            _p = nxtBlockAddr;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        //move to prev block!
        iterator& operator -- () {
            //subtract block size to our block pointer, then subtract 2x sentinels worth to get to prev block
            int prevSentinel = abs(*((int*)(_p-2)));
            _p = (int*)(_p-2);
            _p = (int*)(((char*)_p)-prevSentinel);
            return *this;
        }
        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            iterator x = *this;
            --*this;
            return x;
        }
    };

    // --------------
    // const_iterator
    // --------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            // <your code>
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        //What do we give when dereferencing? The pointer itself? Unsure...
        int& operator * () const {
            return *(_p - 1);
        }

        // -----------
        // operator ++
        // -----------

        //move to next block if exists?
        const_iterator& operator ++ () {
            //add block size to our block pointer, then add 2x sentinels worth to get to next block
            int* nxtBlockAddr = (int*)((char*)(_p + 2) + abs(**this));
            _p = nxtBlockAddr;
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator x = *this;
            ++*this;
            return x;
        }

        // -----------
        // operator --
        // -----------

        //move to prev block if exist?
        const_iterator& operator -- () {
            //subtract block size to our block pointer, then subtract 2x sentinels worth to get to prev block
            int prevSentinel = abs(*((int*)(_p-2)));
            _p = (int*)(_p-2);
            _p = (int*)(((char*)_p)-prevSentinel);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator x = *this;
            --*this;
            return x;
        }
    };

    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * Mark the overall array as a single block
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */
    my_allocator () {
        //(*this)[0] = 0; // replace!
        // <your code>
        if(N < sizeof(T) + 2*sizeof(int))
        {
            throw std::bad_alloc();
        }
        //std::cout << *((int*)a) << std::endl;
        //std::cout << *((int*)(a+(N-4))) << std::endl;
        //std::cout << ((int*)a) << std::endl;
        //std::cout << ((int*)(a+(N-4))) << std::endl;
        //std::cout << sizeof(a) << std::endl;
        //std::cout << N << std::endl;
        *((int*)&a[0]) = N-8;
        *((int*)(&a[N-4])) = N-8;
        //std::cout << *((int*)&a[0]);
        //std::cout << *((int*)&a[N-4]);
        //std::cout << "constructor!";
        //for(int i = 0; i < N; i++)
        //    std::cout << a[i] << std::endl;
        //std::cout << int(a[N-5]) << std::endl;
        //std::cout << N-8 << std::endl;
        assert(valid());
        //std::cout<<"huzzah";
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;



    /*void printShit() const
    {
        std::cout << "PRINTGHING SHIT" << std::endl;
        iterator i((int*)(&a[0])+1);
        while(!this->pastEnd(&*i))
        {
            std::cout << "block is " << *i << std::endl;
            i++;
        }
    }*/

    //edited, not sure if working
    bool pastEnd(int* p) const
    {
        return (long)((char*)(p + 1) + abs(*p)) > (long)&a[N-4];
        //return (long)((char*)(&*c + 1) + abs(*c)) != (long)&a[N-4];
    }

    //edited, not sure if working
    bool pastBeg(int* p) const
    {
        return (long)&a[0] > (long)p;
    }

    bool special(int* p) const
    {
        //std::cout << "special" << std::endl;

        //std::cout << (long)((char*)(p) + abs(*p)) << std::endl;

        //std::cout << (long)&a[N-4] << std::endl;

        return (long)((char*)(p) + abs(*p)) == (long)&a[N-4];
        //return (long)((char*)(&*c + 1) + abs(*c)) != (long)&a[N-4];
    }

    bool atEnd(int* p) const
    {
        //std::cout << "end" << std::endl;

        //std::cout << (long)((char*)(p) + abs(*p)) + 4 << std::endl;

        //std::cout << (long)&a[N-4] << std::endl;

        return (long)((char*)(p) + abs(*p)) + 4 == (long)&a[N-4];
        //return (long)((char*)(&*c + 1) + abs(*c)) != (long)&a[N-4];
    }

    //edited, not sure if working
    bool atBeg(int* p) const
    {
        return (long)&a[0] == (long)(p-1);
    }


    // --------
    // allocate
    // --------
    /**
     * O(1) in space
     * O(n) in time
     * Allocate a block that meets at least min size and input size. Give more if leftover is not enough for full block.
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type n) {
        // <your code>
        //if((char*)_p + abs(**this) != (char*)(a+sizeof(a)-4)) end check
        //if(a != (char*)_p-4) beginning check
        //don't allocate really small blocks...

        n *= sizeof(T);
        //std::cout<<"allocate "<<n;

        if(n < sizeof(T))
        {
            throw std::bad_alloc();
        }

        //search for first block with enough size
        iterator iter = (int*)(&a[0]+4);
        while(true)
        {
            //printShit();
            //std::cout<<"n is "<<n<<std::endl;
            //allocate full block
            if(*iter == (int)(n))
            {
                assert(*iter > 0);
                //this syntax is puke worthy
                int* secSent = (int*)((char*)&*iter + 4 + *iter);
                *secSent *= -1;
                *iter *= -1;
                //must point to actual block, not sentinel
                assert(valid());
                //std::cout << "returning pointer for the block of size  " << *iter << std::endl;
                return (pointer)(&*iter + 1);
            }

            //std::cout << "resultant fuffffff are as follows: " << *iter << std::endl;
            //std::cout << "resultant asdalisdjalksjdasd are as follows: " << (int)(n+8) << std::endl;
            if(*iter > (int)(n))
            {
                assert(*iter > 0);
                //check if leftover size is still enough. If not, give entire block
                if(*iter - (int)n >= sizeof(T) + 2*sizeof(int))
                {
                    //2nd sent of first, and two sents for second block
                    int* firstBreak = (int*)((char*)(&*iter + 1) + n);
                    int* secBreak = firstBreak + 1;
                    int* thirdBreak = (int*)((char*)(&*iter + 1) + *iter);
                    //std::cout << "resultant sentinels are as follows: " << &iter << std::endl;
                    //std::cout << "resultant sentinels are as follows: " << firstBreak << std::endl;
                    //std::cout << "resultant sentinels are as follows: " << secBreak << std::endl;
                    //std::cout << "resultant sentinels are as follows: " << thirdBreak << std::endl;

                    //size of 2nd block, first will be n
                    int secSize = *iter - (int)n - 8;
                    //std::cout << "iter is " << *iter << " n is " << (int)n << std::endl;
                    int nneg = (int)n * -1;
                    //std::cout << "n is " << nneg << " secsize is " << secSize << std::endl;
                    *iter = nneg;
                    *firstBreak = nneg;
                    *secBreak = secSize;
                    *thirdBreak = secSize;
                }
                else
                {
                    //give full block like earlier
                    int* secSent = (int*)((char*)&*iter + 4 + *iter);
                    *secSent *= -1;
                    *iter *= -1;
                }
                //both cases, the block at iter is allocated
                assert(valid());
                //std::cout << "returning pointer for the block of size  " << *iter << std::endl;
                return (pointer)(&*iter + 1);
            }
            if(atEnd(&*iter))
                break;
            ++iter;
            //std::cout << pastEnd(&*iter) << std::endl;
        }

        assert(valid());
        return nullptr;
    }             // replace!

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           // from the prohibition of new

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * Marks a block as free and merges with neighboring free blocks
     * after deallocation adjacent free blocks must be coalesced
     * Coalesce with left and right blocks if necessary
     * throw an invalid_argument exception, if p is invalid
     */



    void deallocate (pointer p, size_type n) {
        // <your code>
        //check if p is invalid (start sentinel matches end sentinel)
        //printShit();
        char* _p = (char*)p;
        //std::cout << *(int*)(_p-4) << std::endl;
        //std::cout << *(int*)(_p + abs(*(int*)(_p-4))) << std::endl;
        if(*(int*)(_p-4) != *(int*)(_p + abs(*(int*)(_p-4))))
        {
            throw std::invalid_argument("sentinels are different values");
        }
        //deallocate
        //do we need to call destroy on the data?

        //mark current sentinels as free
        int* sentinel = (int*)(_p-4);
        assert(*sentinel < 0);
        *sentinel = abs(*sentinel);
        sentinel = (int*)(_p + *sentinel);
        assert(*sentinel < 0);
        *sentinel = abs(*sentinel);
        //check left and merge
        //WE CHECK IF LEFT IS OUT OF BOUNDS!!!
        if(*(int*)(_p-8) > 0 && !atBeg((int*)(_p)))
        {
            //add the sizes, erase the left end and right start, update sentinels
            int* prevFirstSentinel = (int*)((_p - 12) - *(int*)(_p-8));
            int* currLastSentinel = (int*)(_p + *(int*)(_p-4));
            //when merging, first sentinel of prev and last sentinel of current remain. Update these values!
            //we save two middle sentinels as well, so we can add value of prev and curr blocks + 8
            int newSize = *(int*)(_p-8) + *(int*)(_p-4) + 8;
            //std::cout << *(int*)(_p-8) << std::endl;
            //std::cout << *(int*)(_p-4) << std::endl;
            *prevFirstSentinel = newSize;
            *currLastSentinel = newSize;
            //std::cout << *prevFirstSentinel << std::endl;
            //std::cout << *currLastSentinel << std::endl;
            //we need to update p to point to start of new coalesced block
            _p = (char*)(prevFirstSentinel + 1);
            //printShit();
        }
        //check right and merge
        //WE CHECK IF RIGHT IS OUT OF BOUNDS!
        if(*(int*)(_p + *(int*)(_p-4) + 4) > 0 && !special((int*)(_p)))
        {
            //add the sizes, erase the left end and right start, update sentinels
            int* currFirstSentinel = (int*)(_p-4);
            int* nextFirstSentinel = (int*)(_p + *(int*)(_p-4) + 4);
            int* nextLastSentinel = (int*)((char*)(nextFirstSentinel + 1) + *nextFirstSentinel);

            //new size = prev sizes + 2 saved sentinel values
            int newSize = *currFirstSentinel + *nextLastSentinel + 8;
            *currFirstSentinel = newSize;
            *nextLastSentinel = newSize;
            assert(valid());
            //no need to update p now
        }
        //printShit();
        //What happens if there is an update in the middle of an iteration? Is the iterator useless now if it was previously on one of these blocks?
        //don't worry about this, no case where this happens single threaded...

        assert(valid());
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(reinterpret_cast<int*>(&a[4]));
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return iterator(reinterpret_cast<int*>(&a[4]));
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(reinterpret_cast<int*>(&a[N-1]));
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return iterator(reinterpret_cast<int*>(&a[N-1]));
    }
};

#endif // Allocator_h




