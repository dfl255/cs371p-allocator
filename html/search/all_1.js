var searchData=
[
  ['a',['a',['../classmy__allocator.html#acee41867c0875283bf3e67400a7da66c',1,'my_allocator']]],
  ['abs',['abs',['../classmy__allocator.html#afcfc7d47afc409e54a82a44da6fe15c0',1,'my_allocator']]],
  ['allocate',['allocate',['../classmy__allocator.html#ac7d5b846f3bade2e3010f1f1f318ed70',1,'my_allocator']]],
  ['allocator_2ec_2b_2b',['Allocator.c++',['../Allocator_8c_09_09.html',1,'']]],
  ['allocator_2eh',['Allocator.h',['../Allocator_8h.html',1,'']]],
  ['allocator_5fsolve',['allocator_solve',['../Allocator_8c_09_09.html#ad22f871229f64dfb3c0195ff9cd6e2c2',1,'allocator_solve(istream &amp;r, ostream &amp;w):&#160;Allocator.c++'],['../Allocator_8h.html#adbdf53be08c7ec417fe1a3645d00fb5d',1,'allocator_solve(std::istream &amp;r, std::ostream &amp;w):&#160;Allocator.h']]],
  ['allocator_5ftype',['allocator_type',['../structTestAllocator1.html#af833c587251c56fda9cc1169d40545d5',1,'TestAllocator1::allocator_type()'],['../structTestAllocator2.html#a705e5cfbda94b09b29b52b9244a65c1d',1,'TestAllocator2::allocator_type()']]],
  ['atbeg',['atBeg',['../classmy__allocator.html#a2ceb444168b2d1c774dbcc996b37e88a',1,'my_allocator']]],
  ['atend',['atEnd',['../classmy__allocator.html#aebe8364235303c235b030097624c615f',1,'my_allocator']]]
];
