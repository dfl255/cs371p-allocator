# CS371p: Object-Oriented Programming Collatz Repo

* Name: David Lai, Weston Sandland

* EID: dfl434, ws6283

* GitLab ID: dfl255, westonsandland

* HackerRank ID: David_lai_255, westonsandland1

* Git SHA: f12c1a26f7ac51b36fe8c9797fe99ce68172be77

* GitLab Pipelines: https://gitlab.com/dfl255/cs371p-allocator/pipelines

* Estimated completion time: 15 hours

* Actual completion time: 25.0 hours

* Comments: :(
